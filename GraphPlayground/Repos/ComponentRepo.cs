﻿using System;
using System.Collections.Generic;
using GraphPlayground.Models;

namespace GraphPlayground.Repos
{
    public class ComponentRepo
    {
        public List<Component> GetComponents()
        {
            return new()
            {
                new Component() { Title = "Component 1" },
                new Component() { Title = "Component 2" },
                new Component() { Title = "Component 3" },
            };
        }
    }
}
