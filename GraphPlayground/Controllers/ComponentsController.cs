﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphPlayground.Models;
using GraphPlayground.Repos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuikGraph;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GraphPlayground.Controllers
{
    [ApiController]
    [Route("api/components")]
    public class ComponentsController : Controller
    {
        private readonly ComponentRepo componentRepo;

        public ComponentsController(ComponentRepo componentRepo)
        {
            this.componentRepo = componentRepo;
        }

        [HttpGet]
        public IActionResult GetComponents()
        {
            return StatusCode(StatusCodes.Status200OK, new { Result = componentRepo.GetComponents() } );
        }

        [HttpGet("graph")]
        public IActionResult CreateGraph()
        {
            var graph = new AdjacencyGraph<Component, TaggedEdge<Component, string>>();
            var components = componentRepo.GetComponents();

            components.ForEach(c => graph.AddVertex(c));

            var taggedEdge1 = new TaggedEdge<Component, string>(components[0], components[1], "comp1-to-comp2");
            var taggedEdge2 = new TaggedEdge<Component, string>(components[1], components[2], "comp2-to-comp3");

            graph.AddEdge(taggedEdge1);
            graph.AddEdge(taggedEdge2);

            return StatusCode(StatusCodes.Status200OK, new { Result = new { Components = graph.Vertices.ToList() , Edges = graph.Edges.ToList() } });
        }
    }
}
